//创建一个mutations对象
//更改 Vuex 的 store 中的状态的唯一方法是提交 mutation
//mutation中的函数的第一个参数就是当前的state对象
//既然 Vuex 的 store 中的状态是响应式的,Vuex 中的 mutation 也需要与使用 Vue 一样遵守一些注意事项(修改数据的注意事项)
export default {
  increment(state) {
    state.count++;
  },
  //mutation中的函数的第二个参数载荷(payload) 都是接收commit的传参
  //vuex还是希望payload是一个对象,这样可以传递多个参数
  incrementN(state, { n }) {
    state.count += n;
  },
  decrement(state) {
    state.count--;
  },

  setMovieList(state, movieList) {
    state.movieList = movieList;
  },

  //一条重要的原则就是要记住 mutation 必须是同步函数,如果是异步的话,我们无法在工具中追踪数据的变化
  /* incrementWait(state) {
    setTimeout(() => {
      state.count++;
    }, 2000);
  }, */
};
