const vuexStorage = localStorage.getItem("vuex-count")


//创建一个state对象
export default {
count:vuexStorage? JSON.parse(vuexStorage).count : 0,
price:vuexStorage?JSON.parse(vuexStorage).price : 99,
movieList:vuexStorage?JSON.parse(vuexStorage).movieList : []

};
