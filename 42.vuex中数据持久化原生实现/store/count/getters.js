//getters对象
// 有时候我们需要从 store 中的 state 中派生出一些状态，例如对列表进行过滤并计数等等 需要计算，我们就用到了getters 类似于vue中的计算属性
// getter 的返回值会根据它的依赖被缓存起来，且只有当它的依赖值发生了改变才会被重新计算
export default {
  //因为数据是state派生出来的，所以getters中的函数接收一个参数是state
  myMovieList(state) {
    return state.movieList.filter((movie) => {
      return movie.albumName.length < 7 && movie.albumName.length > 3;
    });
  },

  //可以传参的getters
  yourMovieList(state) {
    //如果计算属性在计算的时候，需要接收参数，则getters函数需要返回一个函数
    return ({ start, end }) => {
      return state.movieList.filter((movie) => {
        return movie.albumName.length <= end && movie.albumName.length >= start;
      });
    };
  },
};
