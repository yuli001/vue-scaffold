//1.引入Vue
import Vue from "vue";

//2.引入Vuex
import Vuex from "vuex";

//引入count模块(count小仓库)
import count from "./count";

//3.Vuex其实本质是Vue的一个插件,所以我们要让Vue使用Vuex这个插件
Vue.use(Vuex);

//4.创建一个store(vuex的实例),参数是一个配置对象,在配置对象中可以配置actions,mutations,state,getters等等
//暴露出去供Vue实例的配置项挂载
export default new Vuex.Store({
  //Store方法的配置对象接收一个modules,专门来配置vuex模块化的
  modules: {
    count: count,
  },
});
