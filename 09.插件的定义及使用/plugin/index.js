import Header from "../components/Header"

//插件其实是一个配置对象 配置对象内必须拥有Install方法
//插件被使用 install方法就会被调用

export default {
    //接受一个参数 就是vue构造函数
    install(Vue) {
        //1给所有组件扩展一个属性
        Vue.prototype.hi = "dashagua";

        //2全部扩展一个组件
        Vue.component("Header", Header)

        //3.全局扩展过滤器
        Vue.filter("addEnd",(value)=>{
            return value+"!!!"
        })

        //4.全局指令
        Vue.directive("red",(ele)=>{
            ele.style.color ="red"
        })

    }
}