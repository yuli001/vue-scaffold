import Vue from 'vue'
import App from './App.vue'

//引入插件
import myPlugin from"./plugin"

Vue.config.productionTip = false

//使用插件 要有Vue.use()
Vue.use(myPlugin)

new Vue({
  render: h => h(App),
}).$mount('#app')
