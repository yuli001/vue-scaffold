//1.引入vue
import Vue from "vue";

//2.引入vuex
import Vuex from "vuex";

import axios from "axios";

//创建state对象
const state = {
  count: 0,
  price: 99,
};

/*
创建mutations对象
更改vuex 的store中的对象 唯一的方法是提交mutation
*/
const mutations = {
  increament(state) {
    state.count++;
  },
  increamentN(state, { n }) {
    state.count += n;
  },
  decreament(state) {
    state.count--;
  },
  setMovieList(state, movieList) {
    state.movieList = movieList;
  },
};

/* 
  Action 类似于 mutation，不同在于：
    Action 提交的是 mutation，而不是直接变更状态。
    Action 可以包含任意异步操作。
*/
const actions = {
  //因为actions中一定会调用commit去调用mutation,
  //所以action中的函数接受一个对象,对象中提供一个commit方法
  increamentWait({ commit }) {
    setTimeout(() => {
      //我们不要在action中操作数据
      //当异步完成以后,把数据交给mutation去操作数据
      commit("increament");
    }, 2000);
  },
  increamentNWait({ commit }, payload) {
    setTimeout(() => {
      // console.log(payload, "payload");
      commit("increamentN", payload);
    }, 2000);
  },
  async getMovieList({ commit }) {
    const result = await axios.get(
      "https://pcw-api.iqiyi.com/search/recommend/list?channel_id=1&data_type=1&mode=11&page_id=2&ret_num=48&session=b9fd987164f6aa47fad266f57dffaa6a",
      commit("setMovieList", result.data.data.list)
    );
    commit("setMovieList");
  },
};

const getters = {
  //因为数据是state派生出来的，所以getters中的函数接收一个参数是state
};

//3.Vuex 本质是vue的一个插件 让Vue使用Vuex这个插件
Vue.use(Vuex);
export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters,
});
