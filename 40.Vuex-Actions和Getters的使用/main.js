import Vue from 'vue'
import App from './App.vue'
//引入vuex仓库 store

import store from "./store/store";


//引入插件

Vue.config.productionTip = false

//使用插件 要有Vue.use()

new Vue({
  render: h => h(App),
 store,
}).$mount('#app')
