//1. 引入Vue
import Vue from "vue";

//2. 引入VueRouter
import VueRouter from "vue-router";

//引入路由组件
import Login from "../pages/Login";
import Home from "../pages/Home";

//3. 让Vue使用VueRouter这个插件
Vue.use(VueRouter);

//4.创建路由器对象
export default new VueRouter({
  //在实例化路由器对象的配置中,书写路由表配置
  //组件路径 组件名
  routes: [
    { path: "/login", component: Login },
    { path: "/home", component: Home },
  ],
});
