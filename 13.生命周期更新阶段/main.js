import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false;


new Vue({
  /* 在脚手架中 App最后是通过template渲染在容器中的 */
  render: h => h(App),
}).$mount('#app')