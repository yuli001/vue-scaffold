import state from "./state";
import mutations from "./mutations";
import actions from "./actions";
import getters from "./getters";

export default {
  //重要:打开命名空间
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
