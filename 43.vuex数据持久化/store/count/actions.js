import axios from "axios";
/* 
  Action 类似于 mutation，不同在于：
    Action 提交的是 mutation，而不是直接变更状态。
    Action 可以包含任意异步操作。
*/
export default {
  //因为actions中一定会调用commit去调用mutation,所以action中的函数,接受一个对象,对象中提供一个commit方法
  incrementWait({ commit }) {
    setTimeout(() => {
      //我们不要在action中操作数据
      //当异步完成以后,把数据交给mutation去操作数据
      commit("increment");
    }, 2000);
  },
  incrementNWait({ commit }, payload) {
    // console.log(payload, "payload");
    setTimeout(() => {
      //我们不要在action中操作数据
      //当异步完成以后,把数据交给mutation去操作数据
      commit("incrementN", payload);
    }, 2000);
  },
  async getMovieList({ commit }) {
    const result = await axios.get(
      "https://pcw-api.iqiyi.com/search/recommend/list?channel_id=1&data_type=1&mode=11&page_id=2&ret_num=48&session=b9fd987164f6aa47fad266f57dffaa6a"
    );
    commit("setMovieList", result.data.data.list);
  },
};
