import Vue from 'vue'
import App from './App.vue'
import dayjs from 'dayjs'


Vue.config.productionTip = false

//全局注册过滤器 在所有组件中都可以使用
Vue.filter("formatDate",function(value,format){
  return dayjs(value).format(format)
})

new Vue({
  render: h => h(App),
}).$mount('#app')
