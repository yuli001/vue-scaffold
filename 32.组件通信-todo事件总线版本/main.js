import Vue from 'vue'
import App from './App.vue'

//引入插件

Vue.config.productionTip = false

//使用插件 要有Vue.use()

new Vue({
  render: h => h(App),
  beforeCreate(){
   
    /*
    在beforeCreact中 在Vue原型对象上放一个$bus属性 来保存vm,
    为什么保存vm
    因为$bus的值， 能够被绑自定义事件($on) 和调用自定义自定义事件($emit)
    只有组件实例和vue实例 有这个功能  所以使用vm
    */ 

     //this  vm
    Vue.prototype.$bus=this
  }
}).$mount('#app')
