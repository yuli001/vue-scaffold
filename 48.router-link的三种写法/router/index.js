//1. 引入Vue
import Vue from "vue";

//2. 引入VueRouter
import VueRouter from "vue-router";

//引入路由组件
import Login from "../pages/Login";
import Home from "../pages/Home";
import News from "../pages/Home/News";
import Music from "../pages/Home/Music";

//3. 让Vue使用VueRouter这个插件
Vue.use(VueRouter);

//4.创建路由器对象
export default new VueRouter({
  //在实例化路由器对象的配置中,书写路由表配置
  //组件路径 组件名
  routes: [
    { path: "/login", component: Login },
    {
      path: "/home",
      //在某个路由规则中如果有默认子路由,则在当前路由中不再书写name属性
      component: Home,
      // name: "home",
      children: [
        { path: "music", component: Music, name: "music" },
        { path: "news", component: News, name: "news" },
        /* 二级路由重定向的方式1:redirect也适用于多级路由 */
        { path: "", redirect: "/home/music" },
      ],
    },
    /* 通过路由规则的redirect属性，指定一个新的路由地址，可以很方便地设置路由的重定向 */

    { path: "/", redirect: "/home" },
  ],
});
