import Vue from 'vue'
import App from './App.vue'


Vue.config.productionTip = false

//全局注册过滤器 在所有组件中都可以使用
Vue.directive("bold",(ele)=>{
    ele.style.fontWeight = "bold"
})

new Vue({
  render: h => h(App),
}).$mount('#app')
