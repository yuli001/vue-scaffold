//1.引入vue
import Vue from  "vue"

//2.引入vuex
import Vuex from "vuex"

//3.Vuex 本质是vue的一个插件 让Vue使用Vuex这个插件
Vue.use(Vuex);

//创建state对象
const state = {
    count :0,
}

/*
4.创建一个store （vuex的实例）参数是一个配置对象，
在配置对象中可以配置actions,mutations,state,getters等等
//暴露出去供实例的配置项挂载
*/
export default new Vuex.Store({
    state
})